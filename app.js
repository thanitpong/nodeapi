const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser'); 
const cors = require('cors');
// const multer = require('multer');
// const upload = multer();
// app.use(upload.array()); 

global.__basedir = __dirname;

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const healthRouter = require('./routes/health');
const patientRouter = require('./routes/patient');
const diagnoseRoutes = require("./routes/diagnose");
const initRoutes = require("./routes/files");

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false })); // PARSE application/x-www-form-urlencoded
app.use(bodyParser.json()); // PARSE application/json
// USE STATIC FILES (CSS, JS, IMAGES)
app.use(cors());// allow cors 

// CORS
app.all('/*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// SECURITY
app.disable('x-powered-by');

app.use('/', indexRouter);
app.use('/usersapi', usersRouter);
app.use('/healthapi', healthRouter);
app.use('/patientapi', patientRouter);
app.use('/diagnoseapi', diagnoseRoutes);
app.use('/imagesapi',initRoutes);

app.use('/image', express.static('./resources/uploads'));
//CONNECT ANGULAR
// app.use(express.static(path.join(__dirname, 'dist')));
// app.get('/', (res,req) => {
//   res.sendFile(path.join(__dirname,'dist/index.html'))
// })


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;


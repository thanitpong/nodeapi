const { Pool, Client } = require('pg');
const config = require('../config/db.config');

const pool = new Pool(config);
const client = new Client(config);

module.exports = {pool,client};
const util = require("util");
const multer = require("multer");
const dateFormat = require("dateformat");
const randomstring = require("randomstring");
const fs = require('fs');
const maxSize = 2 * 1024 * 1024;

const date = new Date()
const n_date = dateFormat(date, "Y-m-d H:M:s")

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const visit_id = req.query.visit_id;
    const path = crete_folder(visit_id);
    cb(null, __basedir + "/resources/uploads/visit_"+visit_id);
  },
  filename: (req, file, cb) => {
    const eye = req.query.eye_side;
    const visit_id = req.query.visit_id;
    cb(null, randomstring.generate(7)+"_"+visit_id+"_"+eye+"_"+file.originalname);
  },
});

let uploadFile = multer({
  storage: storage
//   limits: { fileSize: maxSize },
}).single("file");

function crete_folder(visit_id){
    fs.mkdir(__basedir+"/resources/uploads/visit_"+ visit_id, { recursive: true }, function(err) {
        if (err) {
          console.log(err)
        } else {
          console.log("New directory successfully created.")
        }
      })
}

let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = uploadFileMiddleware;
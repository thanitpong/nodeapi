const { Diagnoses } = require("../models/diagnose_model.js");
const jwt = require("jsonwebtoken");
const md5 = require("md5");
var dateFormat = require("dateformat");

const date = new Date();
const emptyObj = {}; // Empty Object
//param = /:id
//query = /func?param1=value&param2.3.4...
//body = bodyjson

index = async (req, res, next) => {
  res.status(200).json({ API: "nodeApi by Fookkiez", APIVersion: "v1.0" });
};

create_diagnose = async (req, res) =>{
  const visit_id = parseInt(req.body.visit_id);
  const users_id = parseInt(req.body.users_id);
  const eye_resource_id = parseInt(req.body.eye_resource_id);
  const diagnose = req.body.diagnose;
  const suggest = req.body.suggest;
  if (visit_id && users_id && eye_resource_id ) {
    await Diagnoses.insert_diagnose(visit_id,users_id,eye_resource_id,diagnose,suggest)
    .then((data) => {
      console.log(data);
      const result = data.rows;
      console.log(result);
      return res.status(200).json({ code: "200", data: result });
    })
    .catch((err) => {
      return res.status(200).json({ code: "400", message: "ไม่สามารถ insert ข้อมูลได้ กรุณาลองเช็ค field อีกครั้ง", error : err });
    });
  }else{
    return res.status(400).json({ code: "400", message: "missing params"});
  }
}

module.exports = {
  index,
  create_diagnose
};

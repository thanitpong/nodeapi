const { Users } = require("../models/users_model.js");
const jwt = require('jsonwebtoken');
const md5 = require("md5");
var dateFormat = require("dateformat");

const date = new Date()
//param = /:id
//query = /func?param1=value&param2.3.4...
//body = bodyjson

index = async (req, res, next) => {
  res.status(200).json({ API: "nodeApi by Fookkiez", APIVersion: "v1.0" });
};

get_user = async (req, res, next) => {
  await Users.get()
    .then((data) => {
      const result = data.rows;
      //   const obj = Object.assign({}, data);
      res.status(200).json({ code: "200", data: result });
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(404).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => res.status(400).json({code: "400", message: "เกิดข้อผิดพลาด",error: err }));
};

get_user_id = async (req, res, next) => {
  const id = parseInt(req.params.id);
  await Users.get_id(id)
    .then((data) => {
      const result = data.rows;
      //   console.log(result)
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(404).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",error: err });
    });
};

login = async (req, res, next) => {
  // const username = req.query.username;
  // const password = req.query.password;
  const {username,password} = req.body;
  if (password != null || password != undefined) {
    const encode = md5(password);
    await Users.post_chk(username, encode)
    .then((data) => {
      const result = data.rows; //return name or detail
      if (result && result.length) {
        let payload = { subject :  result[0].username}
        let token = jwt.sign(payload, 'secretKey')
        res.status(200).json({ code: "200", token, data: result});
      } else {
        res.status(404).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) =>
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",error: err })
    );
  }else{
    res.status(400).json({ code: "400", message: "กรุณาใส่รหัส" })
  }
  
};

register = async (req, res, next) => {
    const {username , password, title_th, firstname_th, lastname_th, status_active, user_type} = req.body;
    const passmd5 = md5(password);
    const type = parseInt(user_type);
    // const n_date = dateFormat(date, "Y-m-d H:M:s")
    await Users.register(username,passmd5,title_th,firstname_th,lastname_th,status_active,date,type)
    .then((data) => {
        let payload = {subject: username}
        let token = jwt.sign(payload, 'secretKey')
        const result = data.rows;
        res.status(201).json({ code: "201", token });
      })
      .catch((err) =>
        res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",error: err  })
      );
};

module.exports = {
  index,
  get_user,
  get_user_id,
  login ,
  register,
};

//     // GET ALL ARTICLES
//     getArticles(req, res, next){
//         Article.get()
//             //.then(data => console.log(data))
//             //.then(data => res.render('index', { data }))
//             .then(data => res.status(200).json({ title: 'Retreived all Articles', success: true, data }))
//             .catch(err => res.status(400).json({ err }));
//     },

//     // CREATE ARTICLE
//     createArticle(req, res, next){
//         // USE BODY PARSER TO EXTRACT DATA FROM CLIENT
//         const { title, content } = req.body;

//         Article.create(title, content)
//             .then(res.status(201).json({ success: true, msg: 'Article Created' }))
//             .catch(err => res.status(400).json({ err }));
//     },

//     // UPDATE ARTICLE
//     updateArticle(req, res, next){
//         // USE BODY PARSER TO EXTRACT DATA FROM CLIENT
//         const { title, content } = req.body;
//         // ID OF ARTICLE TO UPDATE
//         let id = req.params.id;

//         Article.update(title, content, id)
//             .then(res.status(200).json({ success: true, msg: 'Article Updated' }))
//             .catch(err => res.status(400).json({ err }));
//     },

//     // DELETE ARTICLE
//     deleteArticle(req, res, next){
//         let id = req.params.id;

//         Article.delete(id)
//             .then(res.status(200).json({ success: true, msg: 'Article Deleted' }))
//             .catch(err => res.status(400).json({ err }));
//     }

// }

// exports.index = (req, res, next) => {
//   res.status(200).json({
//     data: {
//       name: "Codingincloud",
//       address: {
//         province: "Pathum thani",
//         district: "Mueng Pathum thani",
//       },
//     },
//   });
// };

// getUsers = (request, response) => {
//     pool.query('SELECT * FROM public.user', (error, results) => {
//       if (error) {
//         throw error
//       }
//       response.status(200).json(results.rows)
//     })
//   }

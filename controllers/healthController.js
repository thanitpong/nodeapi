const { Healths } = require("../models/health_model.js");
const jwt = require("jsonwebtoken");
const md5 = require("md5");
var dateFormat = require("dateformat");
const { base_url } = require("../config/config");

const date = new Date();
const emptyObj = {}; // Empty Object
//param = /:id
//query = /func?param1=value&param2.3.4...
//body = bodyjson

index = async (req, res, next) => {
  res.status(200).json({ API: "nodeApi by Fookkiez", APIVersion: "v1.0" });
};

// BEGIN VISIT HN OR VN
all_vn = async (req, res, next) => {
  const begin = req.query.begin;
  const end = req.query.end;
  const vn = parseInt(req.query.vn);
  const name = req.query.name;
  await Healths.get(begin,end,vn,name)
    .then((data) => {
      const result = data.rows;
      //   const obj = Object.assign({}, data);
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) =>
      res
        .status(400)
        .json({ code: "400", message: "เกิดข้อผิดพลาด", error: err })
    );
};

all_vn_doctor = async (req, res, next) => {
  const begin = req.query.begin;
  const end = req.query.end;
  const vn = parseInt(req.query.vn);
  const name = req.query.name;
  await Healths.get_role_doctor(begin,end,vn,name)
    .then((data) => {
      const result = data.rows;
      //   const obj = Object.assign({}, data);
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) =>
      res
        .status(400)
        .json({ code: "400", message: "เกิดข้อผิดพลาด", error: err })
    );
};

get_vn = async (req, res, next) => {
  const vn = parseInt(req.params.vn);
  await Healths.get_vn(vn)
    .then((data) => {
      const result = data.rows;
      //   console.log(result)
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด", error: err });
    });
};

get_hn = async (req, res, next) => {
  const hn = req.params.hn;
  await Healths.get_hn(hn)
    .then((data) => {
      const result = data.rows;
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",  error: err });
    });
};

get_data_va = async (req, res, next) => {
  if (parseInt(req.params.rec)) {
    console.log('vn')
    const rec = parseInt(req.params.rec);
    await Healths.get_va_by_vn(rec)
      .then((data) => {
        const result = data.rows;
        if (result && result.length) {
          res.status(200).json({ code: "200", data: result });
        } else {
          res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
        }
      })
      .catch((err) => {
        res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",  error: err });
      });
  } else {
    console.log('hn')
    const rec = req.params.rec;
    await Healths.get_hn(rec)
      .then((data) => {
        const result = data.rows;
        if (result && result.length) {
          res.status(200).json({ code: "200", data: result });
        } else {
          res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
        }
      })
      .catch((err) => {
        res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",  error: err });
      });
  }
};

get_diagnose = async (req, res, next) => {
  const vn = req.params.vn;
  await Healths.get_diagnose_by_vn(vn)
    .then((data) => {
      const result = data.rows;
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",  error: err });
    });
};

get_detail = async (req, res, next) => {
  const vn = req.params.vn;
  await Healths.get_detail_by_vn(vn)
    .then((data) => {
      const data_return = data.rows;
      data_return.forEach( async(item,index) => {
          data_return[index]['url_image'] = base_url+"image/visit_"+vn+"/"+data_return[index]['resoure_name'];
      });
      const result = data_return;
      console.log(result);
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",  error: err });
    });
};

create_vn = async (req, res, next) => {
  const { hn } = req.body;
  if (hn) {
    //เดียวกลับมาทำเงื่อนไข select มาเช็คก่อนว่าวันนี้มี เสียบไปหรือยัง ถ้าเสียบีตรแล้ว ก็ใช้vnเดิม ท่าไม่มี vn ใหม่
    await Healths.register_vn(hn, date)
      .then((data) => {
        const result = data.rows;
        res.status(200).json({ code: "200", data: result });
      })
      .catch((err) =>
        res
          .status(400)
          .json({ code: "400", message: "เกิดข้อผิดพลาด", error: err })
      );
  } else {
    res
      .status(200)
      .json({ code: "400", message: "กรุณาส่งค่า hn เพื่อเพิ่มข้อมูล" });
  }
};
// END VISIT HN OR VN

// BEGIN VA
va_eye = async (req, res, next) => {
  const { visit_id, va_r, va_l, va_ph_r, va_ph_l, va_gl_r, va_gl_l } = req.body;
  console.log(req.body);
  console.log(visit_id);
  console.log(va_r);
  console.log(va_ph_r);
  console.log(va_gl_r);
  if (visit_id !== undefined) {
    await Healths.insert_va(
      visit_id,
      va_r,
      va_l,
      va_ph_r,
      va_ph_l,
      va_gl_r,
      va_gl_l
    )
      .then((data) => {
        const result = data.rows;
        console.log(result);
        res.status(200).json({ code: "200", data: result });
      })
      .catch((err) =>
        res
          .status(400)
          .json({ code: "400", message: "เกิดข้อผิดพลาด", error: err })
      );
  } else {
    res
      .status(200)
      .json({ code: "400", message: "กรุณาส่งค่า va เพื่อเพิ่มข้อมูล" });
  }
};
//END VA


insertai_cataract = async (req, res) =>{
  const img_id = req.body.img_id;
  const badim_model = req.body.badim_model;
  const ref_model = req.body.ref_model;
  if (img_id && badim_model && ref_model) {
    await Healths.insert_cr(img_id,badim_model,ref_model)
    .then((data) => {
      console.log(data);
      const result = data.rows;
      console.log(result);
      return res.status(200).json({ code: "200", data: result });
    })
    .catch((err) => {
      return res.status(200).json({ code: "400", message: "ไม่สามารถ insert ข้อมูลได้ กรุณาลองเช็ค field อีกครั้ง", error : err });
    });
  }else{
    return res.status(400).json({ code: "400", message: "missing params"});
  }
}

insertai_diabetic = async (req, res) =>{
  const img_id = req.body.img_id;
  const badim_model = req.body.badim_model;
  const ref_model = req.body.ref_model;
  if (img_id && badim_model && ref_model) {
    await Healths.insert_dr(img_id,badim_model,ref_model)
    .then((data) => {
      console.log(data);
      const result = data.rows;
      console.log(result);
      return res.status(200).json({ code: "200", data: result });
    })
    .catch((err) => {
      return res.status(200).json({ code: "400", message: "ไม่สามารถ insert ข้อมูลได้ กรุณาลองเช็ค field อีกครั้ง", error : err });
    });
  }else{
    return res.status(400).json({ code: "400", message: "missing params"});
  }
}



module.exports = {
  index,
  create_vn,
  all_vn,
  get_vn,
  get_hn,
  va_eye,
  get_data_va,
  get_diagnose,
  get_detail,
  insertai_diabetic,
  insertai_cataract,
  all_vn_doctor
};



// filter_data = async (req, res, next) => {
//   const begin = req.query.begin;
//   const end = req.query.end;
//   const vn = parseInt(req.query.vn);
//   const name = req.query.name;
//   await Healths.get_filter_date(begin,end,vn,name)
//     .then((data) => {
//       // const result = data.rows;
//       // console.log(data);
//       //   const obj = Object.assign({}, data);
//       // if (result && result.length) {
//         // res.status(200).json({ code: "200", data: result });
//         res.status(200).json({ code: "200", data: req.params,data2: data });
//       // } else {
//         // res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
//       // }
//     })
//     .catch((err) =>
//       res
//         .status(400)
//         .json({ code: "400", message: "เกิดข้อผิดพลาด", error: err })
//     );
// };
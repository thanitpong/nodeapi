const { Patients } = require("../models/patient_model.js");
const jwt = require('jsonwebtoken');
const md5 = require("md5");
var dateFormat = require("dateformat");
const e = require("express");

const date = new Date()
//param = /:id
//query = /func?param1=value&param2.3.4...
//body = bodyjson

index = async (req, res, next) => {
  res.status(200).json({ API: "nodeApi by Fookkiez", APIVersion: "v1.0" });
};

get_patient = async (req, res, next) => {
  await Patients.get()
    .then((data) => {
      const result = data.rows;
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",error: err }));
};

get_patient_hn = async (req, res, next) => {
  const hn = req.params.hn;
  await Patients.get_hn(hn)
    .then((data) => {
      const result = data.rows;
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",error: err });
    });
};

get_patient_vn = async (req, res, next) => {
  const vn = parseInt(req.params.vn);
  await Patients.get_vn(vn)
    .then((data) => {
      const result = data.rows;
      if (result && result.length) {
        res.status(200).json({ code: "200", data: result });
      } else {
        res.status(200).json({ code: "404", message: "ไม่พบข้อมูล" });
      }
    })
    .catch((err) => {
      res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด",error: err });
    });
};

register_patient = async (req, res, next) => {
  const {hn, cardid, title_th, title_en, firstname_th, firstname_en, lastname_th, lastname_en, dob, card_issue, card_exp, mobile, home_no, village_no, province, district, subdistrict, sex} = req.body;
  console.log(req.body);
  if(hn && cardid && sex){
    const n_dob = dateFormat(dob, "yyyy-mm-dd")
    const n_card_issue = dateFormat(card_issue, "yyyy-mm-dd")
    const n_card_exp = dateFormat(card_exp, "yyyy-mm-dd")
    await Patients.register(hn, cardid, title_th, title_en, firstname_th, firstname_en, lastname_th, lastname_en, n_dob, n_card_issue, n_card_exp, mobile, home_no, village_no, province, district, subdistrict, sex, date)
    .then((data) => {
        const result = data.rows;
        res.status(201).json({ code: "201", data :data });
      })
      .catch((err) =>
        res.status(400).json({ code: "400", message: "เกิดข้อผิดพลาด" , error: err})
      );
  }else{
    res.status(200).json({ code: "400", message: "กรุณาใส่ข้อมูลให้ถูกต้อง"})
  }
  
};

module.exports = {
  index,
  register_patient,
  get_patient,
  get_patient_hn,
  get_patient_vn
};

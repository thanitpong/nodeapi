const uploadFile = require("../middleware/upload");
const fs = require('fs');
const { Files } = require("../models/file_model");
const { base_url } = require("../config/config");
const e = require("express");
const upload = async (req, res) => {
  try {
    await uploadFile(req, res);
    // await getUserInfo(req, res)
    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }
    const image_name = req.file.filename;
    const visit_id = req.query.visit_id;
    const hn = req.query.hn;
    const eye_side = req.query.eye_side;
    if (image_name && visit_id && hn && eye_side) {
        await Files.uploads(image_name, visit_id, hn, eye_side)
        .then((data) => {
          const result = data.rows;
          result[0]['url_image'] = base_url+"image/visit_"+visit_id+"/"+result[0].resoure_name;
          console.log(result);
          return res.status(200).json({ code: "200", data: result });
        })
        .catch((error) => {
          return res.status(200).json({ code: "400", message: "ไม่สามารถ upload รูปได้", error : error });
        });
    }else{
        return res.status(200).json({ code: "400", message: "missing params" });
    }
  } catch (err) {
    res.status(500).send({
      message: `Could not upload the file: ${req.file}. ${err}`,
    });
  }
};


///มาทำต่อ โดยดึงจาก Database/////////
const getListFiles = (req, res) => {
  const visit_id = req.params.vn;
  const directoryPath = __basedir + "/resources/uploads/visit_"+ visit_id;
  fs.readdir(directoryPath, function (err, files) {
    if (err) {
      return res.status(500).send({
        message: "Unable to scan files!",
      });
    }

    console.log(files)
    let fileInfos = [];
    files.forEach((file) => {
      fileInfos.push({
        name: file,
        url: base_url+"image/visit_"+visit_id+"/"+file,
      });
    });
    return res.status(200).send(fileInfos);
  });
};


///มาทำต่อ โดยดึงจาก Database/////////
const getListmedia = async (req, res) => {
  const visit_id = req.params.vn;
  await Files.get_images_vn(visit_id)
  .then((data) => {
    const data_return = data.rows;
    data_return.forEach( (item,index) => {
        data_return[index]['url_image'] = base_url+"image/visit_"+visit_id+"/"+data_return[index]['resoure_name'];
    });
    const result = data_return;
    return  res.status(200).json({ code: "200", data: result });
  })
  .catch((error) => {
    return res.status(200).json({ code: "400", message: "ไม่สามารถโหลดข้อมูลได้เนื่องจากไม่พบ visit_id ที่กำหนด", error : error });
  });

};


const download = (req, res) => {
  const fileName = req.query.fileName;
  const visit_id = req.query.visit_id;
  const directoryPath = __basedir + "/resources/uploads/visit_"+visit_id+"/";

  res.download(directoryPath + fileName, fileName, (err) => {
    if (err) {
      res.status(500).send({
        message: "Could not download the file. " + err,
      });
    }
  });
};

module.exports = {
  upload,
  getListFiles,
  download,
  getListmedia
};

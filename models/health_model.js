const {pool,client} = require('../config/db.connect');

const datenow = new Date()
const b_date  = datenow.getDate()
const e_date = datenow.getDate() +1
const month = datenow.getMonth() + 1
const b_year = datenow.getFullYear() - 1;
const e_year = datenow.getFullYear();
var date_begin = b_year + "-" + month + "-" + b_date;
var date_end = e_year + "-" + month + "-" + e_date;


const Healths = {};
// GET ALL visit role admin or officer
Healths.get = (begin,end,vn,name) => {
    var param_data = '';
    var param_vari = [];
    if(begin != undefined && end != undefined && begin != '' && begin != 'null' && end != '' && end != 'null'){
        d_begin = filter_date_START(begin)
        d_end = filter_date_END(end)
        param_vari.push(d_begin,d_end)
    }else{
        param_vari.push(date_begin,date_end)
    }
    if(!isNaN(vn)){
        param_data += 'and v.id = $3 ' 
        param_vari.push(vn)
        console.log('vn');
    }
    if(name != '' && name != 'null'  && name != undefined){
        param_data += 'and p.firstname_th like $3 or p.lastname_th like $4'
        param_vari.push(name+'%',name+'%')
        console.log('name');
    }
    var  que = 'select v.id ,p.title_th,p.firstname_th,p.lastname_th,v.visit_date ,p.hn,array_agg(d.id) as process '
    +'from public.patient p join public.visit v on v.hn = p.hn '
    +'left join public.diagnose d ON d.visit_id = v.id '
    +'where v.visit_date BETWEEN $1 AND $2 '+ param_data
    +'group by v.id,p.hn,p.title_th,p.firstname_th ,p.lastname_th'
    return pool.query(que, param_vari);
}

// GET ALL visit role DOCTOR
Healths.get_role_doctor = (begin,end,vn,name) => {
    console.log(vn);
    var param_data = '';
    var param_vari = [];
    if(begin != undefined && end != undefined && begin != '' && begin != 'null' && end != '' && end != 'null'){
        d_begin = filter_date_START(begin)
        d_end = filter_date_END(end)
        param_vari.push(d_begin,d_end)
    }else{
        param_vari.push(date_begin,date_end)
    }
    if(!isNaN(vn)){
        param_data += 'and v2.id = $3 ' 
        param_vari.push(vn)
        console.log('vn');
    }
    if(name != '' && name != 'null'  && name != undefined){
        param_data += 'and p.firstname_th like $3 or p.lastname_th like $4'
        param_vari.push(name+'%',name+'%')
        console.log('name');
    }
    var  que = 'select v2.id, p.title_th, p.firstname_th, p.lastname_th, p.hn, v2.visit_date, s.process, array_agg(er2.id) as images_all '
    +'from public.eye_resource er2 left join public.visit v2 on v2.id = er2.visit_id '
    +'inner join patient p on v2.hn  = p.hn '
    +'inner join (select v.id as visit_id,array_agg(d.id) as process from diagnose d '
    +'right join visit v on v.id  = d.visit_id group by v.id) s on s.visit_id = v2.id '
    +'where v2.visit_date BETWEEN $1 AND $2 '+ param_data
    +'group by v2.id,s.process,p.title_th,p.firstname_th ,p.lastname_th ,p.hn '
    return pool.query(que, param_vari);
}

// 
//GET BY_VISIT
Healths.get_vn = (vn_id) => {
    return pool.query('SELECT * FROM public.visit WHERE id = $1', [vn_id]);
}

//GET 
Healths.get_hn = (hn) => {
    return pool.query('SELECT * FROM public.visit WHERE hn = $1', [hn]);
}

//GET 
Healths.get_va_by_vn = (id) => {
    return pool.query('select * from public.eye_va ev inner join public.visit v  on ev.visit_id = v.id where v.id = $1', [id]);
}

//GET 
Healths.get_diagnose_by_vn = (id) => {
    return pool.query('select * from public.diagnose where visit_id = $1', [id]);
}

//GET 
Healths.get_detail_by_vn = (id) => {
    return pool.query('select d.id as diagnose,d.visit_id,u.title_th,u.firstname_th,u.lastname_th,er.id as eye_id,er.hn,er.eye_side,er.resoure_name,d.diagnose,d.suggest,d.date_created from public.diagnose d '
    +'inner join public.eye_resource er on d.eye_resource_id = er.id '
    +'inner join public.users u on d.users_id = u.id where d.visit_id = $1', [id]);
}

// CREATE USER
Healths.register_vn = (hn,date_create) => {
    // return pool.query('INSERT into public.visit(hn, visit_date)' + 'VALUES($1, $2)' + 'RETURNING id,hn', [hn,date_create]);
    return pool.query('INSERT into public.visit(hn, visit_date)' + 'VALUES($1, $2)' + 'RETURNING id,hn', [hn,date_create]);

}

Healths.insert_va = (vn,va_r,va_l,va_ph_r,va_ph_l,va_gl_r,va_gl_l) => {
    return pool.query('INSERT into public.eye_va(visit_id, va_r, va_l, va_ph_r, va_ph_l, va_gl_r, va_gl_l)' 
        + 'VALUES($1, $2, $3, $4, $5, $6, $7)' + 'RETURNING *', [vn,va_r,va_l,va_ph_r,va_ph_l,va_gl_r,va_gl_l]);
}

Healths.insert_cr = (img_id,badim_model,ref_model) => {
    return pool.query('INSERT INTO public.result_ai_cataract (img_id, badim_model, ref_model)' 
        + ' VALUES($1, $2, $3)' + 'RETURNING *', [img_id,badim_model,ref_model]);
}

Healths.insert_dr = (img_id,badim_model,ref_model) => {
    return pool.query('INSERT INTO public.result_ai_dr (img_id, badim_model, ref_model)' 
        + 'VALUES($1, $2, $3)' + 'RETURNING *', [img_id,badim_model,ref_model]);
}

filter_date_START = (date_recive) =>{
    const dt = new Date(date_recive)
    const date  = dt.getDate()
    const month = dt.getMonth() + 1
    const year = dt.getFullYear();
    const date_format = year + "-" + month + "-" + date;
    return date_format
}

filter_date_END = (date_recive) =>{
    const dt = new Date(date_recive)
    const date  = dt.getDate() + 1 
    const month = dt.getMonth() + 1
    const year = dt.getFullYear();
    const date_format = year + "-" + month + "-" + date;
    return date_format
}

check_vn = () => {

}

module.exports = {
    Healths
};





// Healths.get = () => {
//     // return pool.query('select * from public.visit v left join public.eye_va ev on v.id = ev.visit_id left join public.eye_resource er on v.id = er.visit_id ');
//     return pool.query('select v.id ,p.title_th,p.firstname_th,p.lastname_th,v.visit_date ,p.hn,array_agg(d.id) as process '
//     +'from public.patient p join public.visit v on v.hn = p.hn '
//     +'left join public.diagnose d ON d.visit_id = v.id '
//     +'group by v.id,p.hn,p.title_th,p.firstname_th ,p.lastname_th');
// }

// // GET ALL visit role DOCTOR
// Healths.get_role_doctor = () => {
//     // return pool.query('select * from public.visit v left join public.eye_va ev on v.id = ev.visit_id left join public.eye_resource er on v.id = er.visit_id ');
//     return pool.query('select v2.id, p.title_th, p.firstname_th, p.lastname_th, p.hn, v2.visit_date, s.process, array_agg(er2.id) as images_all '
//     +'from public.eye_resource er2 left join public.visit v2 on v2.id = er2.visit_id '
//     +'inner join patient p on v2.hn  = p.hn '
//     +'inner join (select v.id as visit_id,array_agg(d.id) as process from diagnose d '
//     +'right join visit v on v.id  = d.visit_id group by v.id) s on s.visit_id = v2.id '
//     +'group by v2.id,s.process,p.title_th,p.firstname_th ,p.lastname_th ,p.hn ');
// }

// // GET ALL visit Fitter date
// Healths.get_filter_date = (begin,end,vn,name) => {
//     var param_data = '';
//     var param_vari = [];
//     if(begin != undefined && end != undefined && begin != '' && begin != 'null' && end != '' && end != 'null'){
//         d_begin = filter_date_START(begin)
//         d_end = filter_date_END(end)
//         param_vari.push(d_begin,d_end)
//     }else{
//         param_vari.push(date_begin,date_end)
//     }
//     if(!isNaN(vn)){
//         param_data += 'and v.id = $3 ' 
//         param_vari.push(vn)
//         console.log('vn');
//     }
//     if(name != '' && name != 'null'  && name != undefined){
//         param_data += 'and p.firstname_th like $3 or p.lastname_th like $4'
//         param_vari.push(name+'%',name+'%')
//         console.log('name');
//     }
//     console.log(param_vari);
//     var  que = 'select v.id ,p.title_th,p.firstname_th,p.lastname_th,v.visit_date ,p.hn,array_agg(d.id) as process '
//     +'from public.patient p join public.visit v on v.hn = p.hn '
//     +'left join public.diagnose d ON d.visit_id = v.id '
//     +'where v.visit_date BETWEEN $1 AND $2 '+ param_data
//     +'group by v.id,p.hn,p.title_th,p.firstname_th ,p.lastname_th'
//     return pool.query(que, param_vari);
// }

// console.log(param_data);
// console.log(param_vari);
const {pool,client} = require('../config/db.connect');

const Users = {};
// GET ALL ARTICLES
Users.get = () => {
    return pool.query('SELECT * FROM public.users');
}

//GET USER_BY_ID 
Users.get_id = (user_id) => {
    return pool.query('SELECT * FROM public.users WHERE id = $1', [user_id]);
}

//GET USER_BY_NAME 
Users.get_name = (user_name) => {
    return pool.query('SELECT * FROM public.users WHERE username = $1', [user_name]);
}

//CHK USER & PASSWORD 
Users.post_chk = (username,password) => {
    // return pool.query('SELECT username,password FROM public.users WHERE username = $1 AND password = $2', [username,password]);
    return pool.query('SELECT u.id, u.username, u."password" ,u.title_th ,u.firstname_th ,u.lastname_th ,ut.type_name '
    +'FROM public.users u inner join public.user_type ut on u.user_type  = ut.id WHERE username = $1 AND password = $2', [username,password]);

}

// CREATE USER
Users.register = (username,pass_md5,title_th,firstname_th,lastname_th,status_active,activate_date,type_id) => {
    console.log(username,pass_md5,title_th,firstname_th,lastname_th,status_active,activate_date,type_id)
    return pool.query(`INSERT into public.users(username, password, title_th, firstname_th, lastname_th, status_active, activate_date, user_type)` + `VALUES($1, $2, $3, $4, $5, $6, $7, $8)`, [username,pass_md5,title_th,firstname_th,lastname_th,status_active,activate_date,type_id]);
}



module.exports = {
    Users
};


// EMPTY OBJECT
// // USED FOR EXPORTING THE FUNCTIONS BELOW
// const Article = {};
// // CREATE ARTICLE
// Article.create = (title, content) => {
//     return db.none(`INSERT into articles(title, content)` + `VALUES($1, $2)`, [title, content]);
// }

// // GET ALL ARTICLES
// Article.get = () => {
//     return pool.query('SELECT * FROM public.user');
// }

// // UPDATE AN ARTICLE
// Article.update = (title, content, articleID) => {
//     return db.none(`UPDATE articles SET title = $1, content = $2 WHERE id = $3`, [title, content, articleID]);
// }

// // DELETE AN ARTICLE
// Article.delete = articleID => {
//     return db.none(`DELETE from articles WHERE id = $1`, articleID);
// }

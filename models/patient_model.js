const e = require('express');
const {pool,client} = require('../config/db.connect');

const Patients = {};
// GET ALL ARTICLES
Patients.get = () => {
    return pool.query('SELECT * FROM public.patient');
}

// //GET USER_BY_NAME 
// Patients.get_id = (id) => {
//     return pool.query('SELECT * FROM public.patient WHERE id = $1', [id]);
// }

//GET USER_BY_NAME 
Patients.get_hn = (hn) => {
    return pool.query('SELECT * FROM public.patient WHERE hn = $1', [hn]);
}
//GET DATA_Detail BY_VISIT
Patients.get_vn = (vn_id) => {
    return pool.query('SELECT v.id as visit_id,p.* FROM public.patient p join public.visit v on p.hn  = v.hn WHERE v.id = $1', [vn_id]);
}

//CHK USER & PASSWORD 
Patients.post_chk = (username,password) => {
    return pool.query('SELECT username,password FROM public.user WHERE username = $1 AND password = $2', [username,password]);
}

// CREATE Patient
Patients.register = (hn, cardid, ti_th, ti_en, fname_th, fname_en, lname_th, lname_en, dob, card_issue, card_exp, mobile, home_no, village_no, province, district, subdistrict, sex, create_date) => {
        return pool.query(`INSERT INTO public.patient (hn, cardid, title_th, title_en, firstname_th, firstname_en, lastname_th, lastname_en, dob, card_issue, card_exp, mobile, home_no, village_no, province, district, subdistrict, sex, create_date)` 
        + `VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)`+ 'RETURNING *',
        [hn, cardid, ti_th, ti_en, fname_th, fname_en, lname_th, lname_en, dob, card_issue, card_exp, mobile, home_no, village_no, province, district, subdistrict, sex, create_date]);
}



module.exports = {
    Patients
};

    // console.log(hn)
    // console.log(cardid)
    // console.log(ti_th)
    // console.log(ti_en)
    // console.log(fname_th)
    // console.log(fname_en)
    // console.log(lname_th)
    // console.log(lname_en)
    // console.log(dob)
    // console.log(card_issue)
    // console.log(card_exp)
    // console.log(mobile)
    // console.log(home_no)
    // console.log(village_no)
    // console.log(province)
    // console.log(district)
    // console.log(subdistrict)
    // console.log(sex)
    // console.log(create_date)
// EMPTY OBJECT
// // USED FOR EXPORTING THE FUNCTIONS BELOW
// const Article = {};
// // CREATE ARTICLE
// Article.create = (title, content) => {
//     return db.none(`INSERT into articles(title, content)` + `VALUES($1, $2)`, [title, content]);
// }

// // GET ALL ARTICLES
// Article.get = () => {
//     return pool.query('SELECT * FROM public.user');
// }

// // UPDATE AN ARTICLE
// Article.update = (title, content, articleID) => {
//     return db.none(`UPDATE articles SET title = $1, content = $2 WHERE id = $3`, [title, content, articleID]);
// }

// // DELETE AN ARTICLE
// Article.delete = articleID => {
//     return db.none(`DELETE from articles WHERE id = $1`, articleID);
// }

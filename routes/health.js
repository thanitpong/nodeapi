const express = require('express');
const router = express.Router();
const healthController = require('../controllers/healthController');
const verifyController = require('../controllers/verifyController');

/* GET users listing. */
router.get('/', healthController.index);
/* GET users listing. */
router.get('/visit', healthController.all_vn);
// GET LIST doctor 
router.get('/visit/doc_vn', healthController.all_vn_doctor);
// GET detail VA patient vn && hn 
router.get('/visit/va/:rec', healthController.get_data_va);
//GET diagnose ด้วย vn 
router.get('/visit/diagnose/:vn', healthController.get_diagnose);
//GET diagnose detail ด้วย vn 
router.get('/visit/detail_vn/:vn', healthController.get_detail);
// POST 
router.post('/visit/create_vn', healthController.create_vn);
router.post('/visit/va', healthController.va_eye);
//get by hn && vn 
router.get('/visit/hn/:hn', healthController.get_hn);
router.get('/visit/vn/:vn', healthController.get_vn);

// router.get('/visit/filter', healthController.filter_data);
// router.get('/visit/eye/vn/:vn', healthController.get_vn_detail);
// router.get('/visit/profile/hn/:hn', healthController.get_hn_detail);


// ค่าจาก ai ไป
router.post("/cataract", healthController.insertai_cataract);
router.post("/diabetic", healthController.insertai_diabetic);


module.exports = router;

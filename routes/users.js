const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const verifyController = require('../controllers/verifyController');

/* GET users listing. */
router.get('/', userController.index);
// router.get('/', userControllers.index);
/* GET users listing. */
router.get('/user', verifyController, userController.get_user);
router.get('/user/:id', verifyController,userController.get_user_id);
router.post('/login', userController.login);
router.post('/register', userController.register);


module.exports = router;

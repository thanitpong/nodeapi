const express = require("express");
const router = express.Router();
const filecontroller = require("../controllers/filecontroller");

router.post("/upload", filecontroller.upload);
router.get("/imgonserve/:vn", filecontroller.getListFiles);
router.get("/media_visit/:vn", filecontroller.getListmedia);
router.get("/downloads", filecontroller.download);


module.exports = router;

const express = require('express');
const router = express.Router();
const diagnoseController = require('../controllers/diagnoseController');
const verifyController = require('../controllers/verifyController');

/* GET users listing. */
router.get('/', diagnoseController.index);

// POST 
router.post('/create_diagnose', diagnoseController.create_diagnose);


module.exports = router;

const express = require('express');
const router = express.Router();
const patientController = require('../controllers/patientController');
const verifyController = require('../controllers/verifyController');

/* GET users listing. */
router.get('/', patientController.index);
// router.get('/', userControllers.index);
/* GET users listing. */

router.get('/patient', patientController.get_patient);
router.get('/patient/hn/:hn', patientController.get_patient_hn);
router.get('/patient/vn/:vn', patientController.get_patient_vn);
router.post('/register', patientController.register_patient);


module.exports = router;
